
/* Setup Handlebars Helpers */

Handlebars.registerHelper('arrayify', function(obj) {
	var data = [];
	delete obj['_id'];

	var objKeys = _.keys(obj);
	for (var i = 0; i < objKeys.length; i++)
	{
		data.push({
			'key': objKeys[i],
			'value': obj[objKeys[i]]
		});
	}

	return data;
});

Handlebars.registerHelper('FormTextGroup', function(key, value, editing) {

	// console.log('FTG(', key, ', ', value, ', ', editing, ')');

	if (key != '_id')
	{
		var $div = $("<div class='form-group'></div>");
		var $label = $("<span class='form-group-label'></span>");

		var $input;

		if (editing)
			$input = $("<input class='form-control' name='" + key + "' type='text' value='" + value + "'' />");
		else
			$input = $("<input class='form-control' name='" + key + "' type='text' />");

		$div.append($label.text(key));
		$div.append($input.html(value));

		return Simple.Helpers.toHTML($div);
	}
});

Handlebars.registerHelper('Label', function(fullStr, shortStr) {

	var $labelCol = $("<div class='Label col-xs-12 col-sm-2'></div>");
	var $labelSpan = $("<span class='form-group-label'></span>");
	$labelCol.append($labelSpan);

	if (Simple.Helpers.isMobile())
	{
		$labelSpan.text(shortStr);
	}
	else
	{
		$labelSpan.text(fullStr);
	}

	return Simple.Helpers.toHTML($labelCol);
});

Handlebars.registerHelper('List', function(collectionName, keys) {
	
	var coll = DB[collectionName].find().fetch();

	var docs = [];
	for (var i = 0; i < coll.length; i++)
	{
		var docId = coll[i]['_id'];
		delete coll[i]['_id'];

		var columns = [];
		for (var j = 0; j < keys.length; j++)
		{
			columns[j] = {
				'key': keys[j],
				'value': coll[i][keys[j]]
			};
		}

		// if (docId != "ZXB6eXNfwKHqwzZH5")
		if (Simple.Helpers.isNotSampleRecord(collectionName, coll[i]))
		{
			docs.push({
				'id': docId,
				'columns': columns
			});
		}
	}


	return docs;
});

/*
	Thanks! @ Zsolt, http://stackoverflow.com/questions/10128053/is-there-a-way-to-pass-variables-into-templates-in-meteor
*/
Handlebars.registerHelper('partialExtendedContext', function(templateName, dataSrc) {

	var fieldKeys = [];
	var fields; 
	if (dataSrc == 'Assets')
	{
		fields = DB[dataSrc].find({'Type': '_'}).fetch();
	}
	else if (dataSrc == 'Tickets')
	{
		fields = DB[dataSrc].find({'Reporter': '_'}).fetch();
	}

	if (fields.length > 0)
	{
		delete fields[0]['_id'];

		var	keys = [];
		var keysObj = _.keys(fields[0]);
		for (var i = 0; i < keysObj.length; i++)
		{
			var currKey = keysObj[i];
			keys.push(currKey);
		}

		fieldKeys = keys.sort();
	}

	console.log('Keys > ', fieldKeys);

    return new Handlebars.SafeString(Template[templateName]({
    	'CollectionName': dataSrc,
    	'Keys': fieldKeys,
    	'linkName': dataSrc.toLowerCase()
    }));
});
