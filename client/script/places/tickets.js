
Template.Tickets.fieldKeys = function() {

	var assetDocs = Tickets.find({}).fetch();

	if (assetDocs.length > 0)
	{
		delete assetDocs[0]['_id'];
		return _.keys(assetDocs[0]);
	}

	return [];
};
