
Template.Login.events({

	'submit #login-form': function(e, template) {

		e.preventDefault();

		var un = $("input[name='username']").val();
		var pw = $("input[name='password']").val();


		// Verify creds
		var userExists = Simple.Session.verifyUser(un);
		if (userExists)
		{
			Simple.Session.login(un, pw);

			// Route-To: Welcome Page
			Meteor.Router.to('/welcome');
		}
	}

});
