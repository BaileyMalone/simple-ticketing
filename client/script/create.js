
Template.Create.docType = function() {
	return Simple.Session.get('DocType');
};

Template.Create.fields = function() {

	var Fields = [];
	var fieldGroups = [];
	
	console.log('KEYS > ', Simple.Session.get('keys'));

	_.each(Simple.Session.get('keys'), function(field) {
		Fields.push(field.value);
	});
	var fieldKeys = _.keys(Fields);

	for (var i = 0; i < fieldKeys.length; i++)
	{
		fieldGroups.push({
			'key': Fields[fieldKeys[i]]['th'],
			'value': ''
		});
	}

	// console.log('fields > ', )

	return fieldGroups;
};


Template.Create.events({

	"click button[name='create']": function(e, context) {

		// Validate the form as needed
		var formData = Simple.Helpers.prepFormData($("input"));
		var dataValid = Simple.Helpers.validateByDocType(Simple.Session.get('DocType'), formData);

		// Update the record IF formData is valid
		if (dataValid)
		{
			console.log('Valid data! ', formData);
			
			Meteor.call('Create_Asset', formData, function(err) {
				if (err)
				{
					console.log('err > ', err);
				}
				console.log('Updated!');
				Simple.Session.NavTo('assets');
			});
		}
		else
		{
			// Validation Errors!

		}


	}

});
