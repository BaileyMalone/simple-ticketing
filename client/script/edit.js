
Template.Edit.docId = function() {
	return Simple.Session.get('docId');
};

Template.Edit.docType = function() {
	return Simple.Session.get('DocType');
};

Template.Edit.editing = function() {
	if (Simple.Session.get('editing') == 'true')
		return true;

	return false;
};

Template.Edit.fields = function() {
	return Simple.Session.get('edit_data');
};


Template.Edit.events({

	"click button[name='delete']": function(e, context) {
		var docId = Simple.Session.get('docId');

		Meteor.call('Delete_Asset', docId, function(err) {
			if (err)
			{
				console.log('err > ', err);
			}

			Simple.Session.NavTo('assets');
		});
	},

	"click button[name='save']": function(e, context) {

		// Validate the form as needed
		var formData = Simple.Helpers.prepFormData($("input"));
		var dataValid = Simple.Helpers.validateByDocType(Simple.Session.get('DocType'), formData);

		var docId = Simple.Session.get('docId');

		// Update the record IF formData is valid
		if (dataValid)
		{
			console.log('Valid data! ', formData);
			console.log('Id > ', docId);
			
			// Assets.update({_id: docId}, {$set: formData}, function() {
			// 	console.log('Saved!');
			// 	Simple.Session.NavTo('assets');
			// });
			Meteor.call('Update_Asset', docId, formData, function(err) {
				if (err)
				{
					console.log('err > ', err);
				}
				console.log('Updated!');
				Simple.Session.NavTo('assets');
			});
		}
		else
		{
			// Validation Errors!

		}

	}

});
