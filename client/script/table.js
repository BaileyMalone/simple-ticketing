
Template.Table.events({

	'click button.add': function(e, context) {

		var keys = [];
		$('th').each(function() {
			keys.push({
				'th': $(this).text()
			});
		});
		Simple.Session.set('keys', keys);
		Simple.Session.NavTo('assets/add');
	},

	'click tr': function(e, context) {

		var targetData = {};
		var docId = $(e.currentTarget).attr('docId');
		$(e.currentTarget).find('td').each(function() {
			targetData[$(this).attr('name')] = $(this).html();
		});

		// console.log('Target: ', $(e.currentTarget).find('td'));
		// console.log('_id = ', docId);

		// Pull the record so that you can edit all
		// of the fields in the DB documents
			// (which may not necessarily be the same as
			// all the fields on that assets page at the
			// time of edit).
		var result = DB.Assets.find(targetData).fetch();
		if (result.length > 0)
		{
			// Sort the Target Data (so the rows are not all out of order)
			delete result['_id'];

			Simple.Session.set('docId', docId);
			Simple.Session.set('edit_data', result[0]);
		}
		else
		{
			$("div.table-wrapper").append("<p>No record found!</p>");
			return;
		}
		Simple.Session.NavTo('assets/edit/' + docId);
	}	

});
