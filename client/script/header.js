
// Template.TopNav.isDesktop = function() {
// 	return Session.get('isDesktop');
// };

// Template.TopNav.isMobile = function() {
// 	return Session.get('isMobile');
// };

// Template.TopNav.isTablet = function() {
// 	return Session.get('isTablet');
// };


Template['Header-Normal'].events({

	'click li': function(e, template) {

		e.preventDefault();

		if (Simple.Session.loggedIn())
		{
			// Route to Template "Area" //
			var $target = $(event.currentTarget);
			var linkArea = Simple.Helpers.cleanStr($target.find('a').text());

			// console.log('Link Area - ', linkArea);
			// console.log('Template[', Simple.Helpers.capitalizeFirstChar(linkArea) + 'Nav', '] - ', Template[Simple.Helpers.capitalizeFirstChar(linkArea) + 'Nav']());

			// Set Nav Choice for SimpleSession //
			Simple.Session.NavTo(Simple.Helpers.capitalizeFirstChar(linkArea));
		}

	}

});

