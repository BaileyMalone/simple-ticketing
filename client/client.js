
Simple = {};

/* Utility Functions */
Simple.Helpers = {

	'arrayify': function($elements) {
		var inputData = [];
		$elements.each(function() {  inputData.push(this); });

		return inputData;
	},

	// Thank you Steve Harrison!
	// http://stackoverflow.com/questions/1026069/capitalize-the-first-letter-of-string-in-javascript
	'capitalizeFirstChar': function(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	},

	'cleanStr': function(str) {

		if (str)
			return str.toLowerCase().trim();

		return '';
	},

	// Use HTML5 LocalStorage to persist 'isMobile' flag.
	'isMobile': function() {
		if ($(window).width() <= 790)
			localStorage['isMobile'] = true;
		else
			localStorage['isMobile'] = false;

		// console.log('LS: ', localStorage);
		return localStorage['isMobile'];
	},

	'isNotSampleRecord': function(collection, fields) {
		if (collection.toLowerCase() == 'assets')
		{
			if (fields['Type'] == '_')
				return false;
		}
		else if (collection.toLowerCase() == 'tickets')
		{
			if (fields['Reporter'] == '_')
				return false;	
		}

		return true;
	},

	'prepFormData': function($elements) {
		var inputData = {};
		$elements.each(function() {  
			inputData[$(this).attr('name')] = $(this).val(); 
		});

		return inputData;
	},

	'recordExists': function(type, data) {

		if (type == 'Asset')
		{
			return (DB.Assets.find({'Number': data['Number']}).fetch().length > 0);
		}

		return false;
	},

	'toHTML': function($element) {

		var $temp = $("<div></div>");
		$temp.append($element);

		return $temp.html();
	},

	'validateByDocType': function(type, data) {

		if (type.toLowerCase() == 'asset')
		{	
			// No., Type and Status are REQUIRED
			return ((data['Number'].length > 0) && (data['Type'].length > 0) && (data['Status'].length > 0));
		}

		return false;
	}
};

// Session Wrapper (uses HTML5 Web Storage; Chr/FE/Op/IE8+ support) //
/*
	
*/
function SimpleSession() {

	var __constructor = function(that) {
		that.isAdmin = (sessionStorage.getItem('isAdmin')) ? sessionStorage.getItem('isAdmin') : false;
		that.isLoggedIn =  (sessionStorage.getItem('isLoggedIn')) ? sessionStorage.getItem('isLoggedIn') : false;
	}(this);

	this.get = function(key) {

		// console.log('GET(', key, ')');

		if (Meteor.user())
		{
			var value = sessionStorage.getItem(key);

			if (value)
			{
				if (value.indexOf("{") != -1)
				{
					var data = JSON.parse(value);

					var valueArray = [];
					$.each(data, function(key) {
						valueArray.push({
							'key': key,
							'value': data[key]
						});
					});

					return valueArray;
				}

				return value;
			}
		}

		return '';
	};

	this.Id = function() {
		return sessionStorage.getItem('userId');
	};

	this.loggedIn = function() {
		return (Meteor.user() !== null) ? true : false;
	};

	this.login = function(username, password) {
		Meteor.loginWithPassword(username, password);
	};

	this.NavTo = function(name) {
		sessionStorage.setItem('nav', name);
		Meteor.Router.to('/' + name);
	};

	this.set = function(key, value) {

		console.log('SET(', key, ', ', value, ')');

		if ( (Meteor.user()) && (key && (value !== undefined)) )
		{
			if ((typeof value) == 'object')
			{
				// console.log('	setting obj ...', JSON.stringify(value));
				sessionStorage.setItem(key, JSON.stringify(value));
			}
			else
			{
				// console.log('	setting string ...', value);
				sessionStorage.setItem(key, value);
			}
		}
	};

	this.verifyUser = function(username) {
		if (!username)
			return false;

		return (Meteor.users.find({'username': username }).fetch().length > 0) ? true : false;
	};
}
Simple.Session = new SimpleSession();

