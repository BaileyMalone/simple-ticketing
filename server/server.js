/* Access Rules */
// Assets = new Meteor.Collection("assets");
// Tickets = new Meteor.Collection("tickets");

	/*
		<Feature>
			Customizable list of fields for each list of things;
			customizable by EACH USER;
	*/
  DB = {};
  DB.Assets = new Meteor.Collection("assets");
  DB.Tickets = new Meteor.Collection("tickets");

/*
	<Feature>
		Server Access ONLY to DB
			*0* client-side script runs unfiltered in the 
			Node server-container
*/

Meteor.users.allow({

	insert: function() {
		return true;
	},

	update: function() {
		return true;
	}

});

DB.Assets.allow({

	insert: function() {
		return true;
	},

	update: function() {
		return true;
	}
	
});

DB.Tickets.allow({

	insert: function() {
		return true;
	},

	update: function() {
		return true;
	}
	
});


Meteor.methods({

	'Create_Asset': function(data) {
		DB.Assets.insert(data);
	},

	'Delete_Asset': function(docId) {
		DB.Assets.remove({_id: docId});
	},

	'Update_Asset': function(docId, data) {
		DB.Assets.remove({_id: docId});
		DB.Assets.insert(data);
	}

});
