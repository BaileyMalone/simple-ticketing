
if (Meteor.isClient) 
{
  /* Collections */
  // Assets = new Meteor.Collection("assets");
  // Tickets = new Meteor.Collection("tickets");
  DB = {};
  DB.Assets = new Meteor.Collection("assets");
  DB.Tickets = new Meteor.Collection("tickets");

  
  /*
    Docs loaded into header and content,
    ex. seach bar for ticket HUD => new small content panel for searching,
      using the [ticket] nav location as the query base

      -> set an attribute on the HMTL for NAV && ____ elements s.t.
        ~ "data src" ~ Mongo Docs Base query piece
  */
  /* Routing */
  Meteor.Router.add({

    '/': 'Login',
    '/welcome': 'Welcome',
    '/admin': 'Admin',
    '/assets': 'Assets',
      '/assets/add': function() {
        Simple.Session.set('DocType', 'Asset');
        Simple.Session.set('editing', 'false');
        return 'Create';
      },
      '/assets/edit/:id': function(docId) {
        Simple.Session.set('DocType', 'Asset');
        Simple.Session.set('editing', 'true');
        Simple.Session.set('docId', docId);
        return 'Edit';
      },
    '/report': 'Report',
    '/tickets': 'Tickets',
    '/people': 'People',
    '*': '404'

  });

  /* Filtering */
  Meteor.Router.filters({
    // https://github.com/tmeasday/meteor-router#filtering
    'checkLoggedIn': function(page) {

      if (Meteor.user() === null)
      {
        return 'Login';
      }
      else if (Meteor.user() !== null)
      {
        return (page.toLowerCase() == 'login') ? 'Welcome' : page;
      }
      else
      {
        setTimeout(function() {
          Meteor.Router.to('/');
        }, 0);
      }
        
    }

  });
  Meteor.Router.filter('checkLoggedIn');

  Meteor.startup(function() {

    // Determine UI-State //
      // @ Load //
    $(window).load(function() {

      var screenWidth = $(window).width();

      // console.log('width - ', screenWidth);

      if (screenWidth <=  468) // 768) // phone
      {
        // console.log('[MOBILE]');
        Session.set('isDesktop', false);
        Session.set('isMobile', true);
        Session.set('isTablet', false);
      }
      if (768 < screenWidth) // desktop
      {
        // console.log('[DESKTOP]');
        Session.set('isDesktop', true);
        Session.set('isMobile', false);
        Session.set('isTablet', false);
      }
      if (468 < screenWidth && screenWidth <= 768) // tablet
      {
        // console.log('[TABLET]');
        Session.set('isDesktop', false);
        Session.set('isMobile', false);
        Session.set('isTablet', true);
      }
    });

      // @ Resize //
    var isMobileCheck = _.debounce(function(e) {

      var screenWidth = $(window).width();

      if (screenWidth <=  468) // 768) // phone
      {
        // console.log('[MOBILE]');
        Session.set('isDesktop', false);
        Session.set('isMobile', true);
        Session.set('isTablet', false);
      }
      if (768 < screenWidth) // desktop
      {
        // console.log('[DESKTOP]');
        Session.set('isDesktop', true);
        Session.set('isMobile', false);
        Session.set('isTablet', false);
      }
      if (468 < screenWidth && screenWidth <= 768) // tablet
      {
        // console.log('[TABLET]');
        Session.set('isDesktop', false);
        Session.set('isMobile', false);
        Session.set('isTablet', true);
      }
    }, 500);
    $(window).resize(isMobileCheck);
    /* ************************ */

    // Header Scroll Check //
    $(window).scroll(function() {
      if ($(window).scrollTop() > 150)
      {
        Session.set('scrolling', true);
      }
      else
      {
        Session.set('scrolling', false);
      }
    });
    /* ************************ */

  });

}

